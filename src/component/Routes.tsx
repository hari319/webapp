import React from 'react';
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from 'react-router-dom';
import SignIn from './SignIn';
import NotFound from './Roles/NotFound';
import Booking from './BookMeeting/Booking';
// import Selection from './Selection';
// import NewPage from './SelectItem/NewPage';

const authGuard = (Component: any) => () => {
  return localStorage.getItem('token') ? (
    <Component />
  ) : (
    <Redirect to="/login" />
  );
};

const Routes = (props: any) => (
  <Router {...props}>
    <Switch>
      <Route exact path="/">
        <Redirect to="/login" />
      </Route>
      <Route exact path="/login">
        <SignIn />
      </Route>
      <Route exact path="/booking" render={authGuard(Booking)}></Route>
      {/* <Route exact path="/newPage" render={authGuard(NewPage)}></Route>
      <Route exact path="/selection" render={authGuard(Selection)}></Route> */}
      <Route exact path="*" render={authGuard(NotFound)}></Route>
    </Switch>
  </Router>
);
export default Routes;
