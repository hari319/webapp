import React from "react";
import { Button, Grid, useTheme } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { Context } from "../Context";
import { getMeetingListByRole } from "../../api/api";

function RoomSelection() {
  const { t } = useTranslation();
  const theme = useTheme();
  const matchesMobile = useMediaQuery(theme.breakpoints.down("xs"));
  const { setRoom, loginRole, room, setMeetingList } =
    React.useContext(Context);

  function onClick(room: string) {
    setRoom(room);
    getMeetingListByRole(loginRole, room).then((item: any) => {
      setMeetingList(item);
    });
  }

  return (
    <>
      <div
        className="RoomContainer"
        style={{
          width: matchesMobile ? "44%" : "17% ",
        }}
      >
        <Grid
          container
          direction={"row"}
          justify="center"
          alignItems="center"
          wrap="nowrap"
          spacing={1}
        >
          <Grid container item xs={10} className={"BtnMobileView"}>
            <Button
              className="button1"
              onClick={(e) => {
                onClick("room1");
              }}
            >
              {t("ROOM") + " 1"}
            </Button>
          </Grid>
          <Grid container item xs={10} className={"BtnMobileView"}>
            <Button
              className="button2"
              onClick={(e) => {
                onClick("room2");
              }}
            >
              {t("ROOM") + " 2"}
            </Button>
          </Grid>
        </Grid>
      </div>
      <div className="selectTag">
        <p>
          {t("SELECTROOM")} :-{" "}
          {room === "room1"
            ? t("ROOM") + " 1"
            : room === "room2"
            ? t("ROOM") + " 2"
            : ""}
        </p>
      </div>
    </>
  );
}

export default RoomSelection;
