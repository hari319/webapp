/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import {
  withStyles,
  Theme,
  createStyles,
  makeStyles,
} from "@material-ui/core/styles";
import { Context } from "../Context";
import { Grid, Paper, useTheme } from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTranslation } from "react-i18next";
import Dropdown from "./Dropdown";
import { getMeetingListByRole, deleteMeetingByID } from "../../api/api";

export default function MeetingTable() {
  const { meetingList, setMeetingList, loginRole, room } =
    React.useContext(Context);
  const theme = useTheme();
  const matchesMobile = useMediaQuery(theme.breakpoints.down("xs"));
  const { t } = useTranslation();

  const useStyles = makeStyles((theme) => ({
    table: {
      minWidth: 700,
      size: "small",
    },
    divTable: {
      margin: "44px auto 5px auto",
    },
    mobileView: {
      display: matchesMobile ? "contents" : "",
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: "center",
      color: "white",
      width: "100%",
      backgroundColor: "#51504E",
    },
    paperBox: {
      padding: theme.spacing(1),
      textAlign: "center",
      color: theme.palette.text.secondary,
      width: "100%",
      backgroundColor: "white",
      marginBottom: "22px",
    },
    GridMain: {
      marginTop: "22px",
    },
    btnMbl: {
      marginTop: "10px",
      marginBottom: "22px",
    },
  }));

  useEffect(() => {
    getNewMeetingList();
  }, [room]);

  function getNewMeetingList() {
    if (room.length > 0) {
      getMeetingListByRole(loginRole, room).then((item: any) => {
        setMeetingList(item);
      });
    }
  }

  const StyledTableCell = withStyles((theme: Theme) =>
    createStyles({
      head: {
        backgroundColor: "#51504E",
        color: theme.palette.common.white,
      },
      body: {
        fontSize: 14,
      },
    })
  )(TableCell);

  const StyledTableRow = withStyles((theme: Theme) =>
    createStyles({
      root: {
        "&:nth-of-type(odd)": {
          backgroundColor: theme.palette.action.hover,
        },
      },
    })
  )(TableRow);

  const deleteList = (type: any, i: any) => {
    if (type === "edit") {
    } else if (type === "remove") {
      deleteMeetingByID(i).then((response: any) => {
        if (response.ok) {
          getNewMeetingList();
        } else {
          throw new Error("NETWORK RESPONSE NOT OK");
        }
      });
    }
  };

  const classes = useStyles();
  return (
    <>
      {meetingList.length > 0 &&
        room.length > 0 &&
        (!matchesMobile ? (
          <div className={classes.divTable}>
            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="customized table">
                <TableHead>
                  <TableRow>
                    <StyledTableCell align="center">
                      {t("Date")}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {t("STARTTIME")}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {t("ENDTIME")}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {t("ROLE")}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {t("EDIT")}
                    </StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {meetingList.map((row, i) => (
                    <StyledTableRow
                      key={Math.floor(1000 + Math.random() * 9000)}
                    >
                      <StyledTableCell
                        component="th"
                        scope="row"
                        align="center"
                      >
                        {row.date}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.startTime}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.endTime}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {row.role}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        <Dropdown onChange={deleteList} id={row.id} />
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        ) : (
          <Grid
            className={classes.GridMain}
            container
            direction={matchesMobile ? "column" : "row"}
            justify="center"
            alignItems="center"
            wrap="nowrap"
            spacing={1}
          >
            {meetingList.map((row, i) => (
              <>
                <Paper className={classes.paperBox}>
                  <Grid container item xs={10} className={classes.mobileView}>
                    <Paper className={classes.paper}>{t("Date")}</Paper>
                  </Grid>
                  <Grid container item xs={10} className={classes.mobileView}>
                    <p>{row.date}</p>
                  </Grid>
                  <Grid container item xs={10} className={classes.mobileView}>
                    <Paper className={classes.paper}>{t("STARTEND")}</Paper>
                  </Grid>
                  <Grid container item xs={10} className={classes.mobileView}>
                    <p>
                      {row.startTime} - {row.endTime}
                    </p>
                  </Grid>
                  <Grid container item xs={10} className={classes.mobileView}>
                    <Paper className={classes.paper}>{t("ROLE")}</Paper>
                  </Grid>
                  <Grid container item xs={10} className={classes.mobileView}>
                    <p>{row.role}</p>
                  </Grid>
                  <Grid container item xs={10} className={classes.mobileView}>
                    <Paper className={classes.paper}>{t("EDIT")}</Paper>
                  </Grid>
                  <Grid container item xs={10} className={classes.mobileView}>
                    <Dropdown onChange={deleteList} id={row.id} />
                  </Grid>
                </Paper>
              </>
            ))}
          </Grid>
        ))}
    </>
  );
}
