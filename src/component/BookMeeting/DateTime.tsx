/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable array-callback-return */
import "date-fns";
import React from "react";
import { Button, Grid, useTheme } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import { makeStyles } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import moment from "moment";
import { Context } from "../Context";
import { useTranslation } from "react-i18next";
import TimePicker from "./TimePicker";
import updateMeetingList, { getMeetingListByRole } from "../../api/api";

export default function DateTime() {
  const useStyles = makeStyles(() => ({
    row: {
      display: matchesMobile ? "grid" : "flex",
      flexDirection: matchesMobile ? "column" : "row",
      alignItems: "center",
    },
    btn: {
      marginTop: "19px",
      width: matchesMobile ? "80%" : "26%",
    },
    mobileView: {
      display: matchesMobile ? "contents" : "",
    },
  }));

  const { meetingList, setMeetingList, loginRole, room } =
    React.useContext(Context);
  const [selectedDate, setSelectedDate] = React.useState<Date | null>(null);
  const [startTime, setStartTime] = React.useState<Date | null>(null);
  const [endTime, setEndTime] = React.useState<Date | null>(null);
  const theme = useTheme();
  const matchesMobile = useMediaQuery(theme.breakpoints.down("xs"));
  const { t } = useTranslation();
  const classes = useStyles();

  function addRoom() {
     let config = {
       date: moment(selectedDate).format("DD.MM.YYYY"),
       startTime: moment(startTime).format("HH:mm"),
       endTime: moment(endTime).format("HH:mm"),
       role: loginRole,
       room: room,
       id: 0,
     };
    if (getDifference()) {
      getMeetingListByRole("admin", room).then((meetingLists: any) => {
        if (meetingLists.length > 0) {
          meetingLists.forEach((item: any, index: any) => {
            switch (true) {
              case check(item):
                alert(t("ALERTNOROOM"));
                break;
              case index === meetingLists.length - 1 &&
                checkIncludes(meetingLists, config):
                setValue(config);
                break;
            }
          });
        } else {
          setValue(config);
        }
      });
    } else {
      alert(t("NOTAHOUR"));
    }
  }

  function check(item: any) {
    return (
      item.date === moment(selectedDate).format("DD.MM.YYYY") &&
      (isAvailable(startTime, item.startTime, item.endTime) ||
        isAvailable(endTime, item.startTime, item.endTime))
    );
  }

  function checkIncludes(meetingLists: any, config: any) {
    let result = true
    if (meetingLists.length > 0) {      
      meetingLists.forEach((item: any) => {
        if (
          item.date === config.date &&
          item.startTime === config.startTime &&
          item.endTime === config.endTime
        ) {
          result = false;
        }
      });
    }
    return result;
  }

  function getDifference() {
    let startT = moment(moment(startTime).format("HH:mm"), "HH:mm");
    let endT = moment(moment(endTime).format("HH:mm"), "HH:mm");
    let MinsDiff = endT.diff(startT, "minutes");
    return MinsDiff / 60 !== 0 && Number.isInteger(MinsDiff / 60);
  }

  function isAvailable(newTime: any, start: any, end: any) {
    let format = "HH:mm";
    let time = moment(newTime, format),
      beforeTime = moment(start, format),
      afterTime = moment(end, format);
    return time.isBetween(beforeTime, afterTime);
  }

  function setValue(config:any) {
    updateMeetingList(config).then((response: any) => {
      config["id"] = response;
      setMeetingList([...meetingList, config]);      
    });
    setSelectedDate(null);
    setStartTime(null);
    setEndTime(null);
  }

  return (
    <div className={classes.row}>
      {room.length > 0 && (
        <Grid
          container
          direction={matchesMobile ? "column" : "row"}
          justify="center"
          alignItems="center"
          wrap="nowrap"
          spacing={1}
        >
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <Grid container item xs={10} className={classes.mobileView}>
              <KeyboardDatePicker
                margin="normal"
                id="date-picker-dialog"
                label={t("PICKDATE")}
                format="DD.MM.YYYY"
                value={selectedDate}
                onChange={(e) => {
                  setSelectedDate(e);
                }}
                KeyboardButtonProps={{
                  "aria-label": "change date",
                }}
              />
            </Grid>
            <Grid container item xs={10} className={classes.mobileView}>
              <TimePicker
                value={startTime}
                onChange={setStartTime}
                label={"STARTTIME"}
              />
            </Grid>
            <Grid container item xs={10} className={classes.mobileView}>
              <TimePicker
                value={endTime}
                onChange={setEndTime}
                label={"ENDTIME"}
              />
            </Grid>
          </MuiPickersUtilsProvider>
          <Button
            className={classes.btn}
            disabled={
              selectedDate === null || startTime === null || endTime === null
            }
            variant="contained"
            color="primary"
            onClick={addRoom}
          >
            {t("ADD")}
          </Button>
        </Grid>
      )}
    </div>
  );
}
