/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { createContext, useState, FunctionComponent } from "react";

export const Context = createContext<{
  setUser: (value: string) => void;
  setFruits: (value: string) => void;
  setUserName: (value: string) => void;
  setNewList: (value: string[]) => void;
  setMeetingList: (value: any[]) => void;
  setLoginRole: (value: string) => void;
  setRoom: (value: string) => void;
  fruits: string;
  newList: string[];
  meetingList: any[];
  loginRole: string;
  room: string;
}>({
  setUser: () => {},
  setFruits: () => {},
  setUserName: () => {},
  setNewList: () => {},
  setMeetingList: () => {},
  setLoginRole: () => {},
  setRoom: () => {},
  fruits: "",
  newList: [],
  meetingList: [],
  loginRole: "",
  room: "",
});

const ContextProvider: FunctionComponent<any> = ({ children }) => {
  return <Context.Provider value={Auth()}>{children}</Context.Provider>;
};

const Auth = () => {
  const [user, setUser] = useState("");
  const [fruits, setFruits] = React.useState<string>("");
  const [newList, setNewList] = React.useState<string[]>([]);
  const [meetingList, setMeetingList] = React.useState<any[]>([]);
  const [loginRole, setLoginRole] = React.useState<string>("");
  const [room, setRoom] = React.useState<string>("");

  const setUserName = (value: string) => {
    setUser(value);
  };

  return {
    setUser,
    setFruits,
    setUserName,
    setNewList,
    setMeetingList,
    setLoginRole,
    setRoom,
    fruits,
    newList,
    meetingList,
    loginRole,
    room,
  };
};

export default ContextProvider;
