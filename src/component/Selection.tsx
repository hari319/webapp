import React from "react";
import { Button, Grid, useTheme } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useHistory } from "react-router-dom";

function Selection() {
  const { t } = useTranslation();
  const theme = useTheme();
  const matchesMobile = useMediaQuery(theme.breakpoints.down("xs"));
  const history = useHistory();

  function onClick(type: string) {
    history.push(type);
  }

  return (
    <div className="container">
      <Grid
        container
        direction={matchesMobile ? "column" : "row"}
        justify="center"
        alignItems="center"
        wrap="nowrap"
        spacing={1}
      >
        <Grid container item xs={10} className={"BtnMobileView"}>
          <Button
            className="button1"
            size="large"
            onClick={(e) => {
              onClick("/newPage");
            }}
          >
            {t("FRUITS_WIKI")}
          </Button>
        </Grid>
        <Grid container item xs={10} className={"BtnMobileView"}>
          <Button
            className="button2"
            onClick={(e) => {
              onClick("/booking");
            }}
          >
            {t("BOOKING")}
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}

export default Selection;
