import React, { useContext } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { Context } from "../Context";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      display: "block",
      marginTop: theme.spacing(2),
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 173,
    },
    Button: {
      margin: theme.spacing(1),
      minWidth: 120,
      marginTop: "17px",
    },
    row: {
      marginTop: theme.spacing(4),
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
  })
);

function Title() {
  const classes = useStyles();
  const { fruits, setFruits, newList, setNewList } = useContext(Context);
  let Option = "Apple,Mango,Pineapple";
  const { t } = useTranslation();

  return (
    <div className={classes.row}>
      <FormControl className={classes.formControl}>
        <InputLabel key="key" id="demo-controlled-open-select-label">
          {t("SELECT_FRUITS")}
        </InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          value={fruits}
          onChange={(event: any) => {
            setFruits(event.target.value);
          }}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {Option.split(",").map((item: any) => {
            return <MenuItem value={item}>{item}</MenuItem>;
          })}
        </Select>
      </FormControl>
      <FormControl className={classes.Button}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            if (!newList.includes(fruits)) {
            }
            setNewList([...newList, fruits]);
            setFruits("");
          }}
        >
          {t("SUBMIT")}
        </Button>
      </FormControl>
    </div>
  );
}

export default Title;
