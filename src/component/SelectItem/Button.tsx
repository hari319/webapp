import React, { FunctionComponent } from "react";
import Button from "@material-ui/core/Button";
import { useTranslation } from "react-i18next";

interface ButtonProps {
  label: string;
}

export const StringEditor: FunctionComponent<ButtonProps> = ({
  label,
}) => {
  const { t } = useTranslation();
  return (
    <>
      <Button fullWidth variant="contained" color="primary" type="submit">
        {t(label)}
      </Button>
    </>
  );
};

export default StringEditor;
