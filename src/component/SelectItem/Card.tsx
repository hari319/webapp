/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable react/jsx-no-comment-textnodes */
import React, { useContext } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import fruitsJson from "../../translations/fruitsJson.json";
import Grid from "@material-ui/core/Grid";
import { Context } from "../Context";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
      marginRight: "5px",
    },
    media: {
      height: 300,
      display: "block",
      marginLeft: "auto",
      marginRight: "auto",
      width: "100%",
    },
    row: {
      marginTop: theme.spacing(4),
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
  })
);

function title() {
  const classes = useStyles();
  const { newList } = useContext(Context);
  const { t } = useTranslation();

  return (
    <div className={classes.row}>
      {newList &&
        newList.map((listItem: any) => {
          return fruitsJson.map((item: any) => {
            return (
              <div>
                <Grid item xs={12}>
                  {listItem === item.Name && (
                    <Card className={classes.root}>
                      <CardActionArea>
                        <CardMedia
                          className={classes.media}
                          image={item.Image}
                          title={item.Name}
                        />
                        <CardContent>
                          <Typography gutterBottom variant="h5" component="h2">
                            {item.Name}
                          </Typography>
                          <Typography
                            variant="body2"
                            color="textSecondary"
                            component="p"
                          >
                            {item.Description}
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                      <CardActions>
                        <Typography gutterBottom variant="h5" component="h2">
                          {item.Price}
                        </Typography>
                        <a href={item.Link} target="_blank">
                          {t("LEARN_MORE")}
                        </a>
                      </CardActions>
                    </Card>
                  )}
                </Grid>
              </div>
            );
          });
        })}
    </div>
  );
}

export default title;
