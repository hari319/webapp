import { Suspense } from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import Routes from "./component/Routes";
import ContextProvider from "./component/Context";
import "./component/MutliLanguage/i18n";

function App() {
  if (window.performance) {
    if (performance.navigation.type === 1) {
      localStorage.clear();
    }
  }

  return (
    <Suspense fallback="...">
      <ContextProvider>
          <BrowserRouter>
            <Routes />
          </BrowserRouter>
      </ContextProvider>
    </Suspense>
  );
}

export default App;
