
let headers = new Headers();
headers.append('Content-Type', 'application/json');

export default function updateMeetingList(jsonConfig) {
  const fetchPromise = fetch(
    `http://localhost:8080/meeting`,
    {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(jsonConfig),
    }
  ).then(async (response) => {
    if (response.ok) {
        return await response.json();
      } else {
        throw new Error('NETWORK RESPONSE NOT OK');
      }
    }).catch((error) => {
      return error;
    });

  return fetchPromise;
}

export function getMeetingListByRole(role, room) {
  let URL =
    role === 'admin'
      ? `http://localhost:8080/meetings?room=${room}`
            : `http://localhost:8080/meetingByRole?role=${role}&room=${room}`;

  return fetch(URL, {
    method: 'GET',
    headers: headers,
  }).then(async (response) => {
    if (response.ok) {
        return await response.json();
      } else {
        throw new Error('NETWORK RESPONSE NOT OK');
      }
    })
    .catch((error) => {
      throw new Error('FETCH ERROR:', error);
    });
}

export function deleteMeetingByID(id) {
  return fetch(
    `http://localhost:8080/meeting/${id}`,
    {
      method: 'DELETE',
      headers: headers,
    }
  )
      .then((response) => {
      return response;
    })
    .catch((error) => {
      console.error('FETCH ERROR:', error);
    });
}
